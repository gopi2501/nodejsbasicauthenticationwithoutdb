
const express = require('express');
var bodyParser = require('body-parser')
const path = require('path');

//gk's import of fs
var fs = require('fs');


//Init app
const app = express();

var urlencodedParser = bodyParser.urlencoded({ extended: false })

// app.use(express.json());

// json file 
var jsonfile = require('jsonfile');
var file = './users.json';




// load view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


//home route 
app.get('/', function (req, res) {
    res.render('user');
});

// register route
app.get('/views/register', function (req, res) {
    console.log(req.query);
    res.render('register');
});

// app.post('/views/register',urlencodedParser, function(req, res){
//     console.log(req.body);
//     res.render('register', {qs:req.query});
// });

app.post('/views/register', urlencodedParser, function (req, res) {
    console.log('inside post register call');
    var user_name = req.body.username;
    var email = req.body.email;
    var pswd = req.body.psw;
    //res.render('register', {qs:req.query});


    //gk notes 
    // try {
    //     fs.writeFileSync('sync.txt', 'anni', { mode: 0o755 });
    //   } catch(err) {
    //     // An error occurred
    //     console.error(err);
    //   }

    // end of gk notes
    // fs.writeFile('binary.txt', buffer, function(err) {
    //     // If an error occurred, show it and return
    //     if(err) return console.error(err);
    //     // Successfully wrote binary contents to the file!
    //   });
    // var userobject = '{ "name":' + user_name + ', "uemail": ' + email + ', "upswd":' + pswd + '}';
    var userobject = {name: user_name, uemail: email, upswd: pswd};
    var filedata;
    // reading a file
    fs.readFile('users.json', 'utf-8', function (err, buf) {
        console.log(buf.toString());
        filedata = JSON.parse(buf);
        filedata['users'].push(userobject);
        console.log(filedata);
        console.log(typeof filedata);
        console.log(filedata['users']);
        filedata = JSON.stringify(filedata);
        fs.writeFile('users.json', filedata, { flag: 'w' }, function (err, data) {
            if (err) console.log('error while writing the data to the file ' + err);
            else console.log('data has been written succesfully into the file');
        });

    });

    //start writing
    //var obj= {name: user_name, uemail: email, upswd: pswd};

    console.log('inside post register call2');
    // jsonfile.writeFileSync(file, obj,{ flag: 'a' }, function (err) {
    //     if (err) console.error('error while writing to a file' + err)
    //     // console.log("The file was saved!");
    //     // res.end("This message will be sent back to the client!");
    //   });

    //gks logic to write to a file using writeFile
    // fs.writeFile('users.json', obj, { flag: 'a' }, function(err, data){
    //     if(err) console.log('error while writing the data to the file '+ err);
    //     else console.log('data has been written succesfully into the file');
    // });
    // gk - this is a mistake i dont know why but you can send something with response i like using either render or redirect
    //return res.send('login');
    console.log('rendering the login page');
    return res.render('login');

});

//User login route
app.get('/views/login', function (req, res) {
    res.render('login',{
        message: 'Please Enter Credentials'
    });
});

app.post('/views/login', urlencodedParser, function (req, res) {
    var email = req.body.uname;
    var pswd = req.body.psw;
    console.log('login post action being called');
    //res.render('login', { qs: req.query });
    var datarecords;
    fs.readFile('users.json', 'utf-8', function (err, userjsondata) {
        if (err) console.log('Hey I just got an error while reading the data from your file');
        else {
            datarecords = JSON.parse(userjsondata);
            console.log(typeof datarecords);
            console.log(datarecords);
            var founduser = false;
            // var temprecord;
            for (var i = 0; i < datarecords['users'].length; i++) {
                // console.log('email is ' + email);
                // console.log(datarecords['users'][i]);
                // console.log(typeof datarecords['users'][i]);
                // console.log(typeof datarecords['users'][i]['uemail']);
                // temprecord = JSON.parse(datarecords['users'][i]);
                // console.log(temprecord);
                // console.log(temprecord['uemail'] === email);
             if(datarecords['users'][i]['uemail'] === email && datarecords['users'][i]['upswd'] === pswd){
                //if (temprecord['uemail'] === email) {
                    console.log('found the user');
                    founduser = true;
                    res.render('welcome', {
                        name: datarecords['users'][i]['name'],
                        email: datarecords['users'][i]['uemail'],
                        pswd: datarecords['users'][i]['upswd']
                    });
                }
            }
            if (!founduser) {
                console.log('could not find the user in the file');
                res.render('login',{
                    message: 'login failed'
                })
            }
        }
    });
    // var obj = { name: user_name, uemail: email, upswd: pswd };
    // var loggedInUser = { email: email, upswd: pswd };
    //console.dir(jsononfile.readFileSync(file))

    // jsonfile.readFileSync(file, obj,{ flag: 'a' }, function (err) {
    //     if (err) console.error(err)
    //      console.log("The file was saved!");
    //     // res.end("This message will be sent back to the client!");
    //   });
    // console.log(req.body);
    // res.render('login', {qs:req.query});
});


// start server
app.listen(3000, function () {
    console.log('Server started on port 3000...')
});